import time
import sys
sys.path.append('/home/pi/camaradecultivo')

import infraestructure.persistence.riego_DAO as riego_DAO

from application.comunicacion_serial_arduino import *

import RPi.GPIO  as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#Riego
in1 = 25 #Relé de la bomba de agua

GPIO.setup(in1, GPIO.OUT)


def medir_humedad_suelo():
    humedad_suelo = ""
    # Definir la cadena que se enviará a Arduino
    stringToSend = "D1"
   
    # Escribir la cadena hacia Arduino
    ser.write(stringToSend.encode())
    
    # Wait until we recieve back the same number of bytes as we sent
    # Esperar hasta recibir la respuesta del arduino
    
    while(ser.inWaiting()):
    # Leer la cadena recibida en el buffer de entrada serial
        stringRecieved = ser.readline()
        
        #Decodificar la cadena
        humedad_suelo = stringRecieved.decode('utf-8').rstrip()

    if len(humedad_suelo) == 0:
        humedad_suelo = "0"
              
    riego_DAO.insertar_humedad_suelo(humedad_suelo)
    

def medir_nivel_agua():
    nivel_agua = ""
    # Definir la cadena que se enviará a Arduino
    stringToSend = "D2"
   
    # Escribir la cadena hacia Arduino
    ser.write(stringToSend.encode())
    
    # Wait until we recieve back the same number of bytes as we sent
    # Esperar hasta recibir la respuesta del arduino
    
    while(ser.inWaiting()):
    # Leer la cadena recibida en el buffer de entrada serial
        stringRecieved = ser.readline()
        
        #Decodificar la cadena
        nivel_agua = stringRecieved.decode('utf-8').rstrip()
        
    if len(nivel_agua) == 0:
        nivel_agua = "0"
             
    riego_DAO.insertar_nivel_agua(nivel_agua)


#Función para encender o apagar los dispositivos de riego
def administrar_riego(orden_riego):
    rele = 99
    estado = False
    
    if not orden_riego:
        orden_riego = 11
    else:
        orden_riego = 10
    
    actuador_riego = int(orden_riego/10)
    potencia_riego = int(orden_riego%10)
    
    #Encendido (True) o apagado (False)
    if potencia_riego > 0:
        estado = True
    else:
        estado = False
        
    #Bomba de agua
    if actuador_riego == 1:
        rele = in1
        nombre_riego = "Bomba de agua"
    
    #estado debe ser booleano True o False
    GPIO.output(rele, estado)
    
    riego_DAO.insertar_estado_riego(nombre_riego, potencia_riego)

administrar_riego(False)
time.sleep(1)

# try:
#     while True:
#         medir_nivel_agua()
#         time.sleep(5)
#         
# except KeyboardInterrupt:
#     pass
