import sys
sys.path.append('/home/pi/camaradecultivo')

import application.procedimientos_iluminacion as procedimientos_iluminacion
import application.procedimientos_ventilacion as procedimientos_ventilacion
import application.procedimientos_calefaccion as procedimientos_calefaccion
import application.procedimientos_riego as procedimientos_riego

import infraestructure.persistence.iluminacion_DAO as iluminacion_DAO
import infraestructure.persistence.ventilacion_DAO as ventilacion_DAO
import infraestructure.persistence.calefaccion_DAO as calefaccion_DAO
import infraestructure.persistence.riego_DAO as riego_DAO
import infraestructure.persistence.ptd_DAO as ptd_DAO

import presentation.api.comunicacion_ptd as comunicacion_ptd

import time
import datetime
import schedule

def administrar_modulos():
    estado_gestion_automatica = ptd_DAO.consultar_estado_gestion_automatica()
    
    if int(estado_gestion_automatica.valor):
        
        gestionar_iluminacion()
        time.sleep(1)
        
        gestionar_ventilacion()
        time.sleep(1)
        
        gestionar_calefaccion()
        time.sleep(1)
        
        gestionar_riego()
        time.sleep(1)

    return     
    
    
def gestionar_iluminacion():
    fecha_actual = datetime.datetime.now()
    
    incidencia_luz = iluminacion_DAO.consultar_incidencia_luz()
    estado_luminaria = iluminacion_DAO.consultar_estado_luminaria()
       
    if fecha_actual.hour > 5 and fecha_actual.hour < 19:
        if incidencia_luz.valor <= 230:
            if int(estado_luminaria.valor) < 1:
                procedimientos_iluminacion.administrar_iluminacion(11)        
    else:
        if incidencia_luz.valor > 230:
            if int(estado_luminaria.valor) > 0:
                procedimientos_iluminacion.administrar_iluminacion(10)
    
    #Alerta
    if fecha_actual.hour > 6 and fecha_actual.hour < 19:
        if incidencia_luz.valor <= 230:
            if int(estado_luminaria.valor) > 0:
                mensaje_alerta = "Luminaria fuera de servicio."
                fecha_actual = datetime.datetime.now()
                comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
    else:
        if incidencia_luz.valor > 230:
            if int(estado_luminaria.valor) < 1:
                mensaje_alerta = "Luminaria activa fuera de horario."   
                fecha_actual = datetime.datetime.now()
                comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
                
    return


def gestionar_ventilacion():
    emergencia = False
    estado = 0
    ventilador_a = 1
    ventilador_b = 2
    ventilador_c = 3
    
    apertura_puerta = ventilacion_DAO.consultar_apertura_puerta()
    
    if int(apertura_puerta.valor) < 1:    
        mensaje_alerta = "Puerta principal abierta."   
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
        
    concentracion_gas = ventilacion_DAO.consultar_concentracion_gas()
    
    if int(concentracion_gas.valor) < 300:
        emergencia = True
        estado = True
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_a)
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_b)
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_c)
        
    if int(concentracion_gas.valor) >= 380:
        emergencia = False
        estado = False
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_a)
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_b)
        procedimientos_ventilacion.administrar_abanicos(estado, ventilador_c)  
    
    
    if emergencia == False:
        fecha_actual = datetime.datetime.now()
        
        if fecha_actual.hour % 2 == 0:
            estado = True
            if fecha_actual.minute <= 5:
                procedimientos_ventilacion.administrar_abanicos(estado, ventilador_a)
            else:
                procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_a)
            procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_b)
            procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_c)    
        else:
            estado = True
            procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_a)        
            if fecha_actual.minute <= 5:
                procedimientos_ventilacion.administrar_abanicos(estado, ventilador_b)
            else:
                procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_b)            
            procedimientos_ventilacion.administrar_abanicos(not estado, ventilador_c)        
       
    if estado:
        estado = 1
    else:
        estado = 0

    estado_abanico_a = ventilacion_DAO.consultar_estado_abanico(ventilador_a)
    estado_abanico_b = ventilacion_DAO.consultar_estado_abanico(ventilador_b)
    estado_abanico_c = ventilacion_DAO.consultar_estado_abanico(ventilador_c)

    if int(estado_abanico_a.valor) != estado:
        mensaje_alerta = "Problema con el ventilador izquierdo."   
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual) 
    
    if int(estado_abanico_b.valor) != estado:
        mensaje_alerta = "Problema con el ventilador derecho."   
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual) 
    
    if int(estado_abanico_c.valor) != estado:
        mensaje_alerta = "Problema con el extractor."   
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual) 
    
    return

    
def gestionar_calefaccion():
    ventilador_c = 3
    
    fecha_actual = datetime.datetime.now()
    
    temperatura_ambiental = calefaccion_DAO.consultar_temperatura_ambiental()
    
    tapete_calefactor = 3
    
    estado_tapete_calefactor = calefaccion_DAO.consultar_estado_calefaccion()
    
    if fecha_actual.hour > 5 and fecha_actual.hour < 19:
        if int(temperatura_ambiental.valor) < 25:
            if int(estado_tapete_calefactor.valor) < 1:
                orden_calefaccion = 1
                procedimientos_calefaccion.administrar_calefaccion(orden_calefaccion+(tapete_calefactor*10))
        
        if int(temperatura_ambiental.valor) > 30:       
            if int(estado_tapete_calefactor.valor) > 0:
                orden_calefaccion = 0
                procedimientos_calefaccion.administrar_calefaccion(orden_calefaccion+(tapete_calefactor*10))                
                                       
    else:
        if int(temperatura_ambiental.valor) < 23:           
            if int(estado_tapete_calefactor.valor) < 1:
                orden_calefaccion = 1
                procedimientos_calefaccion.administrar_calefaccion(orden_calefaccion+(tapete_calefactor*10))
        
        if int(temperatura_ambiental.valor) > 26:       
            if int(estado_tapete_calefactor.valor) > 0:
                orden_calefaccion = 0
                procedimientos_calefaccion.administrar_calefaccion(orden_calefaccion+(tapete_calefactor*10))    
    
    if fecha_actual.hour > 5 and fecha_actual.hour < 19:
        if int(temperatura_ambiental.valor) < 23:
            mensaje_alerta = "Temperatura demasiado baja."   
            fecha_actual = datetime.datetime.now()
            comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)    
        
        if int(temperatura_ambiental.valor) > 32:
            mensaje_alerta = "Temperatura demasiado alta."   
            fecha_actual = datetime.datetime.now()
            comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
            procedimientos_ventilacion.administrar_abanicos(True, ventilador_c)  
        else:
            procedimientos_ventilacion.administrar_abanicos(False, ventilador_c)         
                                       
    else:
        if int(temperatura_ambiental.valor) < 21:
            mensaje_alerta = "Temperatura demasiado baja."   
            fecha_actual = datetime.datetime.now()
            comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
        
        if int(temperatura_ambiental.valor) > 28:
            mensaje_alerta = "Temperatura demasiado alta."   
            fecha_actual = datetime.datetime.now()
            comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
            procedimientos_ventilacion.administrar_abanicos(True, ventilador_c)  
        else:
            procedimientos_ventilacion.administrar_abanicos(False, ventilador_c)  
    
    return


riego_programado = False    
def gestionar_riego():
    emergencia = False
    bomba_de_agua = 3
    
    fecha_actual = datetime.datetime.now()
    
    estado_tanque = riego_DAO.consultar_estado_tanque()
    
    if estado_tanque == 0:
        mensaje_alerta = "Tanque de reserva vacio."   
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)   
     
    humedad_suelo = riego_DAO.consultar_humedad_suelo()
    
    if int(humedad_suelo.valor) > 220:
        emergencia = True
        orden_riego = 1
            
        procedimientos_riego.administrar_riego(orden_riego+(bomba_de_agua*10)) 
        time.sleep(60)
            
        orden_riego = 0
        procedimientos_riego.administrar_riego(orden_riego+(bomba_de_agua*10))
    else:
        emergencia = False
        
    if int(humedad_suelo.valor) < 180:     
        mensaje_alerta = "Humedad excesiva en el suelo."
        fecha_actual = datetime.datetime.now()
        comunicacion_ptd.transmitir_alerta(mensaje_alerta, fecha_actual)
    
    if fecha_actual.day % 2 == 1:
        if fecha_actual.hour == 8:
            if riego_programado == False: 
                orden_riego = 1
                    
                procedimientos_riego.administrar_riego(orden_riego+(bomba_de_agua*10)) 
                time.sleep(60)
                    
                orden_riego = 0
                procedimientos_riego.administrar_riego(orden_riego+(bomba_de_agua*10))
                riego_programado = True 
        if riego_programado == True:
            if fecha_actual.hour > 8:
                riego_programado = False
            
    return


schedule.every(5).minutes.do(administrar_modulos)

try:
    while True:
        schedule.run_pending() 
        time.sleep(1) 
except KeyboardInterrupt:
    pass
