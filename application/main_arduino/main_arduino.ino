#define RELE1 8 //Definir el pin # para el rele del ventilador izquierdo
#define RELE2 7 //Definir el pin # para el rele del ventilador derecho
#define RELE3 4 //Definir el pin # para el rele del extractor izquierdo

void medirIncidenciaLuz();
void medirConcentracionGas();
void administrarAbanicos(int ordenVentilacion); //Función para controlar un abanico específico
void abanicoEncendido(int abanico, bool estado, int velocidad); //Función para cambiar el estado del abanico
void administrarRiego(int ordenRiego);
void medirHumSuelo();
void medirNivelAgua();

String orden = "";  //Cadena para recibir las órdenes envidas a los módulos
String instruccion = "";
int ordenRiego = 0; //Acción que acompaña la orden enviada al modulo de riego
int ordenVentilacion = 0; //Acción que acompaña la orden enviada al modulo de ventilacion
int ordenIluminacion = 0; //Acción que acompaña la orden enviada al modulo de iluminacion

void setup() {
  pinMode(RELE1, OUTPUT); 
  pinMode(RELE2, OUTPUT); 
  pinMode(RELE3, OUTPUT); 

  Serial.begin(250000); // open serial port, set the baud rate as 9600 bps
}

void loop() {
  // Verificar si hay bytes desponibles en el buffer de la entrada serial
  if (Serial.available()>0) 
  {
    //Leer la orden recibida través del puerto serial
    instruccion = Serial.readStringUntil('\n');

    switch(instruccion[0]){
      case 'A':
        orden = instruccion.substring(1);
        ordenIluminacion = orden.toInt();
          if(ordenIluminacion == 0){
            medirIncidenciaLuz();
          }
          
      case 'B': 
        orden = instruccion.substring(1);
        ordenVentilacion = orden.toInt();
        if(ordenVentilacion < 10){
          
          if(ordenVentilacion == 0){
            medirConcentracionGas();
          } 
          
        } else {
          administrarAbanicos(ordenVentilacion);
        }
        break;
        
      case 'D':
        orden = instruccion.substring(1);
        ordenRiego = orden.toInt();
        administrarRiego(ordenRiego);
        
        break;
        
      default:
        Serial.println("X");
        break;
    }
    
  }
 
}

///////////////////////
//Módulo de iluminación (A)
void medirIncidenciaLuz(){
  int incidenciaLuz = 0;
  
  incidenciaLuz = analogRead(2); //connect sensor to Analog 2
  Serial.println(incidenciaLuz); //print the value to serial port
  delay(100);
}

///////////////////////
//Módulo de ventilación (B)
void medirConcentracionGas(){
  int concentracionGas = 0;
  
  concentracionGas = analogRead(0); //connect sensor to Analog 1
  Serial.println(concentracionGas); //print the value to serial port
  delay(100);
}


void administrarAbanicos(int ordenVentilacion) 
{
  int abanico = 0;
  bool estado = LOW;
  int potencia = 0;
  
  //Determinar el abanico a controlar
  abanico = ordenVentilacion/10;

  //Determinar la velocidad del abanico
  potencia = ordenVentilacion%10;

  //Determinar el estado del abanico
  if(potencia>0){
      estado = HIGH;
    } else {
      estado = LOW;
    }
  
  Serial.println(potencia);
   
  //Cambiar el estado del abanico
  abanicoEncendido(abanico, estado);
  delay(100); 
}


void abanicoEncendido(int abanico, bool estado)
{
  //estado debe ser HIGH o LOW
  //abanico debe ser un entero
  int rele = 99;
  
  switch(abanico){
    case 1:
      rele = RELE1;
      break;
    case 2:
      rele = RELE2;
      break;
    case 3:
      rele = RELE3;
      break;
  }
  
  digitalWrite(rele,estado); 
  delay(100);
}


//////////////////
//Módulo de riego (D)

void administrarRiego(int ordenRiego){
  if (ordenRiego == 1) {
    medirHumSuelo();
  }
  if (ordenRiego == 2) {
    medirNivelAgua();
  }  
  delay(100);
}

void medirHumSuelo(){
  /*
Dry: (520 430]260
Wet: (430 350]
Water: (350 260]
*/

  int humedadSuelo = 0;
  
  humedadSuelo = analogRead(1); //connect sensor to Analog 0
  humedadSuelo = map(humedadSuelo,250,520,0,100);
  Serial.println(humedadSuelo); //print the value to serial port
  delay(100);
}

void medirNivelAgua(){
  int nivelAgua = 0;
  
  nivelAgua = analogRead(3); //connect sensor to Analog 0
  Serial.println(nivelAgua); //print the value to serial port
  delay(100);
}
