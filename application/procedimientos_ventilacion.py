import time
import sys
sys.path.append('/home/pi/camaradecultivo')

import infraestructure.persistence.ventilacion_DAO as ventilacion_DAO

from application.comunicacion_serial_arduino import *

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

detector_magnetico = 4

GPIO.setup(detector_magnetico, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def detectar_apertura_puerta():
    estado_puerta = GPIO.input(detector_magnetico)
    
    ventilacion_DAO.insertar_apertura_puerta(estado_puerta)
    

def medir_concentracion_gas():
    concentracion_gas = "0"
    # Definir la cadena que se enviará a Arduino
    stringToSend = "B0"
   
    # Escribir la cadena hacia Arduino
    ser.write(stringToSend.encode())
    
    # Wait until we recieve back the same number of bytes as we sent
    # Esperar hasta recibir la respuesta del arduino 
    while(ser.inWaiting()):
    # Leer la cadena recibida en el buffer de entrada serial
        stringRecieved = ser.readline()
        
        #Decodificar la cadena
        concentracion_gas = stringRecieved.decode('utf-8').rstrip()
        
    if len(concentracion_gas) == 0:
        concentracion_gas = "0"
    
    ventilacion_DAO.insertar_concentracion_gas(concentracion_gas)
    

def administrar_abanicos(estado, abanico):
    estado_ventilador_arduino = "0"
    
    if not estado:
        potencia = 1
    else:
        potencia = 0
        
    orden_ventilacion = (abanico*10)+potencia

    # Definir la cadena que se enviará a Arduino
    # B1 = Abanico 1 Ventilador izquierdo
    # B2 = Abanico 2 Ventilador derecho
    # B3 = Abanico 3 Extractor superior
    
    stringToSend = "B" + str(orden_ventilacion)
   
    # Escribir la cadena hacia Arduino
    ser.write(stringToSend.encode())
    
    while(ser.inWaiting()):
    # Leer la cadena recibida en el buffer de entrada serial
        stringRecieved = ser.readline()
        
        #Decodificar la cadena
        estado_ventilador_arduino = int(stringRecieved.decode('utf-8').rstrip())
    
    if abanico == 1:
        nombre_ventilador = "Ventilador A"
    if abanico == 2:
        nombre_ventilador = "Ventilador B"
    if abanico == 3:
        nombre_ventilador = "Ventilador C"
    
    ventilacion_DAO.insertar_estado_ventilador(nombre_ventilador, estado_ventilador_arduino)

administrar_abanicos(False, 1)
time.sleep(2)
administrar_abanicos(False, 2)
time.sleep(2)
administrar_abanicos(False, 3)
time.sleep(2)
administrar_abanicos(False, 1)
time.sleep(2)

# try:
#     while True:
#         administrar_abanicos(False, 1)
#         time.sleep(5)  
#         administrar_abanicos(False, 2)
#         time.sleep(5)  
#         administrar_abanicos(False, 3)
#         time.sleep(5)    
#         administrar_abanicos(False, 1)
#         time.sleep(5)         
# except KeyboardInterrupt:
#     pass
#     