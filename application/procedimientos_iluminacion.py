import time
import sys
sys.path.append('/home/pi/camaradecultivo')

import infraestructure.persistence.iluminacion_DAO as iluminacion_DAO

from application.comunicacion_serial_arduino import *

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#Luminarias
in1 = 23 #Relé de la lámpara
GPIO.setup(in1, GPIO.OUT)

#Sensor Ultrasónico HC-SR04
GPIO_TRIGGER = 17
GPIO_ECHO = 27
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def medir_incidencia_luz():
    incidencia_luz = ""
    
    # Definir la cadena que se enviará a Arduino
    stringToSend = "A0"
   
    # Escribir la cadena hacia Arduino
    ser.write(stringToSend.encode())
    
    # Wait until we recieve back the same number of bytes as we sent
    # Esperar hasta recibir la respuesta del arduino
    
    while(ser.inWaiting()):
    # Leer la cadena recibida en el buffer de entrada serial
        stringRecieved = ser.readline()
        
        #Decodificar la cadena
        incidencia_luz = stringRecieved.decode('utf-8').rstrip()
        
    if len(incidencia_luz) == 0:
        incidencia_luz = "0"

    iluminacion_DAO.insertar_incidencia_luz(incidencia_luz)
    

def medir_altura_luminaria():
    altura_luminaria = 0
    
    #Distancia entre la luminaria y el follaje de la planta
    
    # Activar Trigger
    GPIO.output(GPIO_TRIGGER, True)
 
    # Desactivar Trigger después de 0.01ms
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
 
    tiempo_inicio = time.time()
    tiempo_final = time.time()
 
    # guardar tiempo_inicio
    while GPIO.input(GPIO_ECHO) == 0:
        tiempo_inicio = time.time()
 
    # guardar time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        tiempo_final = time.time()
 
    # tiempo transcurrido entre el inicio y recepción de la señal
    tiempo_transcurrido = tiempo_final - tiempo_inicio
    # se multiplica por la velocidad del sonido (34300 cm/s)
    # y se divide entre 2, por la ida y vuelta de la señal
    altura_luminaria = (tiempo_transcurrido * 34300) / 2

    iluminacion_DAO.insertar_altura_luminaria(round(altura_luminaria, 2))
    

#Función para encender o apagar las lámparas de cultivo
def administrar_iluminacion(orden_iluminacion):
    rele = 99
    
    if orden_iluminacion:
        orden_iluminacion = 11
    else:
        orden_iluminacion = 10
    
    actuador = int(orden_iluminacion/10)
    potencia = int(orden_iluminacion%10)
    
    estado_luminaria = False #Estado de la luminaria False - apagado y True - encendido

    #Encendido (True) o apagado (False)
    if potencia > 0:
        estado_luminaria = not True
    else:
        estado_luminaria = not False
    
    #Lámpara de cultivo 
    if actuador == 1:
        rele = in1
    
    #estado debe ser booleano True o False
    GPIO.output(rele, estado_luminaria)
    
    iluminacion_DAO.insertar_estado_luminaria(potencia)

administrar_iluminacion(False)
time.sleep(1)


# try:
#     while True:
#         medir_altura_luminaria()
#         time.sleep(5)
# 
# except KeyboardInterrupt:
#     pass
