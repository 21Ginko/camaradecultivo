import time
import sys
sys.path.append('/home/pi/camaradecultivo')

import infraestructure.persistence.calefaccion_DAO as calefaccion_DAO

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

import Adafruit_DHT
#Sensor de temperatura y humedad DHT
DHT_SENSOR = Adafruit_DHT.DHT11 
DHT_PIN = 22

#Calefacción
in1 = 11 #Relé de la cama caliente

GPIO.setup(in1, GPIO.OUT)


#Función para medir temperatura en el aire
def medir_temperatura_humedad_ambiental():
    temperatura = 0
    humedad = 0
    
    humedad, temperatura = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

    calefaccion_DAO.insertar_temperatura_humedad_ambiental(temperatura, humedad)
    

#Función para encender o apagar los dispositivos de calefaccion
def administrar_calefaccion(orden_calefaccion):
    rele_calefaccion = 99
    estado_calefaccion = False
    
    if not orden_calefaccion:
        orden_calefaccion = 11
    else:
        orden_calefaccion = 10
    
    actuador_calefaccion = int(orden_calefaccion/10)
    potencia_calefaccion = int(orden_calefaccion%10)
    
    #Encendido (True) o apagado (False)
    if potencia_calefaccion > 0:
        estado_calefaccion = True
    else:
        estado_calefaccion = False
    
    estado_tapete_calefactor = False #Estado del cama caliente False - apagado y True - encendido
    
       #Tapete calefactor
    if actuador_calefaccion == 1:
        rele_calefaccion = in1
        estado_tapete_calefactor = estado_calefaccion
        nombre_calefaccion = "Tapete calefactor"

    #estado debe ser booleano True o False
    GPIO.output(rele_calefaccion, estado_calefaccion)
    
    calefaccion_DAO.insertar_estado_calefaccion(nombre_calefaccion, potencia_calefaccion)

administrar_calefaccion(False)
time.sleep(1)

# try:
#     while True:
#         medir_temperatura_humedad_ambiental()
#         time.sleep(5)
# 
# except KeyboardInterrupt:
#     pass
