import serial

portname_start = '/dev/ttyACM'

# Probar abrir el puerto serial /dev/ttyACM0->9
for i in range (0, 10):
    # Cambiar i a string
    portnum = str(i)    
    # Añadir "i" al final de portname_start                                       
    portname_full=''.join([portname_start,portnum])
    # intentar abrir el puerto serial culminado en i, si falla tratar de nuevo con i+1
    try:
        ser = serial.Serial(portname_full, 250000, timeout =1)
        ser.flush()
        break
    except:
        # Si se alcanza que i==9 y no se abre ningun puerto, salir e imprimir un mensaje de error
        if (i==9):
            print("No se encontró ningun puerto serial")
            sys.exit(0)