import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer, Float
from sqlalchemy.orm import relationship


class Parametro_ambiental(Base):
    __tablename__ = 'parametro_ambiental'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    valor_minimo = Column(Float)
    valor_maximo = Column(Float)
    
    sensores = relationship("Sensor", back_populates="parametro_ambiental")
    actuadores = relationship("Actuador", back_populates="parametro_ambiental")

    def __init__(self, nombre, valor_minimo, valor_maximo):
        self.nombre = nombre
        self.valor_minimo = valor_minimo
        self.valor_maximo = valor_maximo