import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Sensor(Base):
    __tablename__ = 'sensor'

    id = Column(Integer, primary_key=True)
    modulo_id = Column(Integer, ForeignKey('modulo.id'))
    parametro_ambiental_id = Column(Integer, ForeignKey('parametro_ambiental.id'))
    nombre = Column(String)
    
    modulo = relationship("Modulo", back_populates="sensores")
    parametro_ambiental = relationship("Parametro_ambiental", back_populates="sensores")

    def __init__(self, modulo, parametro_ambiental, nombre):
        self.modulo = modulo
        self.parametro_ambiental = parametro_ambiental
        self.nombre = nombre