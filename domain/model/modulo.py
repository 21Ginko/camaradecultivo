import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship


class Modulo(Base):
    __tablename__ = 'modulo'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)

    sensores = relationship("Sensor", back_populates="modulo")
    actuadores = relationship("Actuador", back_populates="modulo")
    
    def __init__(self, nombre):
        self.nombre = nombre