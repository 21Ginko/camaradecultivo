import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Actuador(Base):
    __tablename__ = 'actuador'

    id = Column(Integer, primary_key=True)
    modulo_id = Column(Integer, ForeignKey('modulo.id'))
    parametro_ambiental_id = Column(Integer, ForeignKey('parametro_ambiental.id'))
    nombre = Column(String)
    
    modulo = relationship("Modulo", back_populates="actuadores")
    parametro_ambiental = relationship("Parametro_ambiental", back_populates="actuadores")

    def __init__(self, modulo, parametro_ambiental, nombre):
        self.modulo = modulo
        self.parametro_ambiental = parametro_ambiental
        self.nombre = nombre