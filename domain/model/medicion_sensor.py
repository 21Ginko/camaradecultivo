import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship


class Medicion_sensor(Base):
    __tablename__ = 'medicion_sensor'

    id = Column(Integer, primary_key=True)
    sensor_id = Column(Integer, ForeignKey('sensor.id'))
    fecha_hora = Column(DateTime(timezone=True), server_default=func.now())
    valor = Column(String)
    
    sensor = relationship("Sensor", backref="mediciones_sensor")
    
    def __init__(self, sensor, valor):
        self.sensor = sensor
        self.fecha_hora = func.now()
        self.valor = valor