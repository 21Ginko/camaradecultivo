import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import Base

from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship


class Estado_actuador(Base):
    __tablename__ = 'estado_actuador'

    id = Column(Integer, primary_key=True)
    actuador_id = Column(Integer, ForeignKey('actuador.id'))
    fecha_hora = Column(DateTime(timezone=True), server_default=func.now())
    valor = Column(String)
    
    actuador = relationship("Actuador", backref="estados_actuador")
    
    def __init__(self, actuador, valor):
        self.actuador = actuador
        self.fecha_hora = func.now()
        self.valor = valor