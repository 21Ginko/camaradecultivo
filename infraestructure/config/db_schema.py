# coding=utf-8

#1 - Importar las clases que representan las tablas de la base de datos
import sys
sys.path.append('/home/pi/camaradecultivo')

from infraestructure.config.postgresql_base import *
from domain.model.modulo import Modulo
from domain.model.parametro_ambiental import Parametro_ambiental
from domain.model.sensor import Sensor
from domain.model.medicion_sensor import Medicion_sensor
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador

# 2 - Generar esquema de la base de datos
Base.metadata.create_all(engine)

# 3 - Apertura de una nueva sesión
session = Session()

# 4 - Creación de registros de tipo modulo para la cámara de cultivo
iluminacion = Modulo("Iluminacion")
ventilacion = Modulo("Ventilacion")
calefaccion = Modulo("Calefaccion")
riego = Modulo("Riego")
procesamiento_transmision_datos = Modulo("Procesamiento y transmision de datos")

# 5 - Definición de registros de tipo parametro_ambiental
temperatura_ambiental = Parametro_ambiental("Temperatura °C en el aire", 20, 40)
humedad_ambiental = Parametro_ambiental("Porcentaje de humedad en el aire", 80, 90)
humedad_suelo = Parametro_ambiental("Porcentaje de humedad en el suelo", 75, 100)
luminosidad = Parametro_ambiental("Magnitud de iluminación", 350, 750)
apertura_puerta = Parametro_ambiental("Estado de la puerta", 0, 1)
concentracion_gas = Parametro_ambiental("Concentración de CO2", 2000, 3000)
nivel_agua = Parametro_ambiental("Nivel del agua", 5, 15)
altura_luminaria = Parametro_ambiental("Altura de la luminaria en cm", 3, 20)
gestion_automatica = Parametro_ambiental("Gestión automatica del control ambiental", 0, 1)

# 6 - Definición de registros de tipo sensor
fotoresistencia = Sensor(iluminacion, luminosidad, "Fotoresistencia")
HCSR04 = Sensor(iluminacion, altura_luminaria, "Ultrasonico HC-SR04")
detector_magnetico = Sensor(ventilacion, apertura_puerta, "Detector switch magnetico")
MQ135 = Sensor(ventilacion, concentracion_gas, "Calidad de aire MQ-135")
DHT11a = Sensor(calefaccion, temperatura_ambiental, "Sensor de temperatura DHT11")
DHT11b = Sensor(calefaccion, humedad_ambiental, "Sensor de humedad ambiental DHT11")
SEN0193 = Sensor(riego, humedad_suelo, "DFRobot SEN0193")
medidor_agua = Sensor(riego, nivel_agua, "Sensor de nivel de agua")

# 7 - Definición de registros de tipo actuador
LED = Actuador(iluminacion, luminosidad, "Lampara LED") 
ventilador_a = Actuador(ventilacion, concentracion_gas, "Ventilador A") 
ventilador_b = Actuador(ventilacion, concentracion_gas, "Ventilador B") 
ventilador_c = Actuador(ventilacion, concentracion_gas, "Ventilador C") 
tapete_calefactor = Actuador(calefaccion, temperatura_ambiental, "Tapete calefactor") 
bomba_de_agua = Actuador(riego, nivel_agua, "Bomba de agua")
raspberry_pi_b3 = Actuador(procesamiento_transmision_datos, gestion_automatica, "Raspberry Pi B3+")

# 8 - Definición de registros de tipo medicion_sensor
medicion_fotoresistencia = Medicion_sensor(fotoresistencia, 0)
medicion_HCSR04 = Medicion_sensor(HCSR04, 0)
medicion_detector_magnetico = Medicion_sensor(detector_magnetico, 0)
medicion_MQ135 = Medicion_sensor(MQ135, 0)
medicion_DHT11a = Medicion_sensor(DHT11a, 0)
medicion_DHT11b = Medicion_sensor(DHT11b, 0)
medicion_SEN0193 = Medicion_sensor(SEN0193, 0)
medicion_medidor_agua = Medicion_sensor(medidor_agua, 0)

# 8 - Definición de registros de tipo estado_actuador
estado_LED = Estado_actuador(LED, 0)
estado_ventilador_a = Estado_actuador(ventilador_a, 0)
estado_ventilador_b = Estado_actuador(ventilador_b, 0)
estado_ventilador_c = Estado_actuador(ventilador_c, 0)
estado_tapete_calefactor = Estado_actuador(tapete_calefactor, 0)
estado_bomba_de_agua = Estado_actuador(bomba_de_agua, 0)
estado_raspberry_pi_b3 = Estado_actuador(raspberry_pi_b3, 0)

# 9 - Almacenamiento de los registros definidos para alimentar las tablas de la base de datos
session.add(iluminacion)
session.add(ventilacion)
session.add(calefaccion)
session.add(riego)
session.add(procesamiento_transmision_datos)

session.add(temperatura_ambiental)
session.add(humedad_ambiental)
session.add(humedad_suelo)
session.add(luminosidad)
session.add(apertura_puerta)
session.add(concentracion_gas)
session.add(nivel_agua)
session.add(altura_luminaria)
session.add(gestion_automatica)

session.add(fotoresistencia)
session.add(HCSR04)
session.add(detector_magnetico)
session.add(MQ135)
session.add(DHT11a)
session.add(DHT11b)
session.add(SEN0193)
session.add(medidor_agua)

session.add(LED)
session.add(ventilador_a)
session.add(ventilador_b)
session.add(ventilador_c)
session.add(tapete_calefactor)
session.add(bomba_de_agua)
session.add(raspberry_pi_b3)

session.add(medicion_fotoresistencia)
session.add(medicion_HCSR04)
session.add(medicion_detector_magnetico)
session.add(medicion_MQ135)
session.add(medicion_DHT11a)
session.add(medicion_DHT11b)
session.add(medicion_SEN0193)
session.add(medicion_medidor_agua)

session.add(estado_LED)
session.add(estado_ventilador_a)
session.add(estado_ventilador_b)
session.add(estado_ventilador_c)
session.add(estado_tapete_calefactor)
session.add(estado_bomba_de_agua)
session.add(estado_raspberry_pi_b3)

#Confirmación y cierre de la transacción con la base de datos
session.commit()
session.close()
