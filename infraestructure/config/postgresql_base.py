# coding=utf-8

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#Conexión con el servidor de base de datos
engine = create_engine('postgresql://pi:12345@localhost:5432/camaradecultivo')
Session = sessionmaker(bind=engine)

Base = declarative_base()