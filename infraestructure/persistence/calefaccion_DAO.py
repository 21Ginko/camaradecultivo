import sys
sys.path.append('/home/pi/camaradecultivo')

from sqlalchemy import desc
from infraestructure.config.postgresql_base import Session
from domain.model.sensor import Sensor
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador
from domain.model.medicion_sensor import Medicion_sensor


def insertar_temperatura_humedad_ambiental(temperatura, humedad):
    session = Session()
    
    DHT11a = session.query(Sensor).filter(Sensor.nombre == 'Sensor de temperatura DHT11').first()
    medicion_DHT11a = Medicion_sensor(DHT11a, temperatura)
    DHT11b = session.query(Sensor).filter(Sensor.nombre == 'Sensor de humedad ambiental DHT11').first()
    medicion_DHT11b = Medicion_sensor(DHT11b, humedad)
    
    session.add(medicion_DHT11a)
    session.add(medicion_DHT11b)
    session.commit()
    session.close()


def consultar_temperatura_ambiental():
    session = Session()
    temperatura_ambiental = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Sensor de temperatura DHT11').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return temperatura_ambiental


def consultar_humedad_ambiental():
    session = Session()
    humedad_ambiental = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Sensor de humedad ambiental DHT11').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return humedad_ambiental


def insertar_estado_calefaccion(nombre_calefaccion, potencia_calefaccion):
    session = Session()
    Calefaccion = session.query(Actuador).filter(Actuador.nombre == nombre_calefaccion).first()
    estado_actuador = Estado_actuador(Calefaccion, potencia_calefaccion)
    session.add(estado_actuador)
    session.commit()
    session.close()
    
    
def consultar_estado_calefaccion():   
    session = Session()
    estado_calefaccion = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == 'Tapete calefactor').order_by(desc(Estado_actuador.id)).first()
    session.close()
            
    return estado_calefaccion