import sys
sys.path.append('/home/pi/camaradecultivo')

from sqlalchemy import desc
from infraestructure.config.postgresql_base import Session
from domain.model.sensor import Sensor
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador
from domain.model.medicion_sensor import Medicion_sensor


def insertar_apertura_puerta(estado_puerta):
    session = Session()
    Detector_magnetico = session.query(Sensor).filter(Sensor.nombre == 'Detector switch magnetico').first()
    medicion_sensor = Medicion_sensor(Detector_magnetico, estado_puerta)
    session.add(medicion_sensor)
    session.commit()
    session.close()
    
    
def consultar_apertura_puerta():
    session = Session()
    apertura_puerta = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Detector switch magnetico').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return apertura_puerta


def insertar_concentracion_gas(concentracion_gas):
    session = Session()
    MQ135 = session.query(Sensor).filter(Sensor.nombre == 'Calidad de aire MQ-135').first()
    medicion_sensor = Medicion_sensor(MQ135, concentracion_gas)
    session.add(medicion_sensor)
    session.commit()
    session.close()


def consultar_concentracion_gas():
    session = Session()
    concentracion_gas = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Calidad de aire MQ-135').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return concentracion_gas


def insertar_estado_ventilador(nombre_ventilador, estado_ventilador_arduino):
    session = Session()
    Ventilador = session.query(Actuador).filter(Actuador.nombre == nombre_ventilador).first()
    estado_actuador = Estado_actuador(Ventilador, estado_ventilador_arduino)
    session.add(estado_actuador)
    session.commit()
    session.close()
    
    
def consultar_estado_abanico(abanico):
    if abanico == 1:
        nombre_ventilador = 'Ventilador A'
    if abanico == 2:
        nombre_ventilador = 'Ventilador B'
    if abanico == 3:
        nombre_ventilador = 'Ventilador C'
    
    session = Session()   
    estado_abanico = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == nombre_ventilador).order_by(desc(Estado_actuador.id)).first()
    session.close()
    
    return estado_abanico
 