import sys
sys.path.append('/home/pi/camaradecultivo')

from sqlalchemy import desc
from infraestructure.config.postgresql_base import Session
from domain.model.sensor import Sensor
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador
from domain.model.medicion_sensor import Medicion_sensor    
 
 
def insertar_humedad_suelo(humedad_suelo):
    session = Session()
    SEN0193 = session.query(Sensor).filter(Sensor.nombre == 'DFRobot SEN0193').first()
    medicion_sensor = Medicion_sensor(SEN0193, humedad_suelo)
    session.add(medicion_sensor)
    session.commit()
    session.close()


def consultar_humedad_suelo():
    session = Session()
    humedad_suelo = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'DFRobot SEN0193').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return humedad_suelo


def insertar_nivel_agua(estado_nivel_agua):
    session = Session()
    medidor_agua = session.query(Sensor).filter(Sensor.nombre == 'Sensor de nivel de agua').first()
    medicion_sensor = Medicion_sensor(medidor_agua, estado_nivel_agua)
    session.add(medicion_sensor)
    session.commit()
    session.close()
    
    
def consultar_nivel_agua():
    session = Session()
    nivel_agua = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Sensor de nivel de agua').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return nivel_agua


def consultar_estado_tanque():
    estado_tanque = 0
    
    nivel_agua = consultar_nivel_agua()
    
    if int(nivel_agua.valor) <= 200:
       estado_tanque = 0
    else:
        if int(nivel_agua.valor) > 200  and int(nivel_agua.valor) <=700:
            estado_tanque = 1
        else:
            estado_tanque = 2
           
    return estado_tanque

    
def insertar_estado_riego(nombre_riego, potencia_riego):
    session = Session()
    Riego = session.query(Actuador).filter(Actuador.nombre == nombre_riego).first()
    estado_actuador = Estado_actuador(Riego, potencia_riego)
    session.add(estado_actuador)
    session.commit()
    session.close()
    

def consultar_estado_riego():          
    session = Session()   
    estado_riego = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == 'Bomba de agua').order_by(desc(Estado_actuador.id)).first()
    session.close()
            
    return estado_riego