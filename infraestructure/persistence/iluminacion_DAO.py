import sys
sys.path.append('/home/pi/camaradecultivo')

from sqlalchemy import desc
from infraestructure.config.postgresql_base import Session
from domain.model.sensor import Sensor
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador
from domain.model.medicion_sensor import Medicion_sensor


def insertar_incidencia_luz(incidencia_luz):
    session = Session()
    Fotoresistencia = session.query(Sensor).filter(Sensor.nombre == 'Fotoresistencia').first()
    medicion_sensor = Medicion_sensor(Fotoresistencia, incidencia_luz)
    session.add(medicion_sensor)
    session.commit()
    session.close()


def consultar_incidencia_luz():
    session = Session()
    incidencia_luz = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Fotoresistencia').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return incidencia_luz


def insertar_altura_luminaria(altura_luminaria):
    session = Session()
    HCSR04 = session.query(Sensor).filter(Sensor.nombre == 'Ultrasonico HC-SR04').first()
    medicion_sensor = Medicion_sensor(HCSR04, altura_luminaria)
    session.add(medicion_sensor)
    session.commit()
    session.close()


def consultar_altura_luminaria():
    session = Session()
    altura_luminaria = session.query(Medicion_sensor).join(Sensor).filter(Sensor.nombre == 'Ultrasonico HC-SR04').order_by(desc(Medicion_sensor.id)).first()
    session.close()
    
    return altura_luminaria


def consultar_orden_luminaria():
    session = Session()
    estado_luminaria = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == 'Lampara LED').order_by(desc(Estado_actuador.id)).first()
    session.close()
    
    return int(estado_luminaria.valor)


def insertar_estado_luminaria(estado):
    session = Session()
    LED = session.query(Actuador).filter(Actuador.nombre == 'Lampara LED').first()
    estado_actuador = Estado_actuador(LED, estado)
    session.add(estado_actuador)
    session.commit()
    session.close()


def consultar_estado_luminaria():
    session = Session()
    estado_luminaria = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == 'Lampara LED').order_by(desc(Estado_actuador.id)).first()
    session.close()

    return estado_luminaria
