import sys
sys.path.append('/home/pi/camaradecultivo')

from sqlalchemy import desc
from infraestructure.config.postgresql_base import Session
from domain.model.actuador import Actuador
from domain.model.estado_actuador import Estado_actuador


def insertar_estado_gestion_automatica(estado):
    session = Session()
    raspberry_pi_b3 = session.query(Actuador).filter(Actuador.nombre == 'Raspberry Pi B3+').first()
    estado_actuador = Estado_actuador(raspberry_pi_b3, estado)
    session.add(estado_actuador)
    session.commit()
    session.close()


def consultar_estado_gestion_automatica():
    session = Session()
    estado_gestion_automatica = session.query(Estado_actuador).join(Actuador).filter(Actuador.nombre == 'Raspberry Pi B3+').order_by(desc(Estado_actuador.id)).first()
    session.close()
    
    return estado_gestion_automatica
