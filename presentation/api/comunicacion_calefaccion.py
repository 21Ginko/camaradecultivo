import sys
sys.path.append('/home/pi/camaradecultivo')

import application.procedimientos_calefaccion as procedimientos_calefaccion

import infraestructure.persistence.calefaccion_DAO as calefaccion_DAO

from infraestructure.config.thingsboard_access_data import THINGSBOARD_HOST, CALEFACCION_ACCESS_TOKEN

import paho.mqtt.client as mqtt
import json

DHT11_data = {'temperatura_ambiental': 0, 'humedad_ambiental': 0}
calefaccion_data = {'estado_calefaccion': 0}

cliente_calefaccion = mqtt.Client()

cliente_DHT11 = mqtt.Client()
cliente_tapete_calefactor = mqtt.Client()


def transmitir_temperatura_humedad_ambiental():
    # Set access token
    cliente_DHT11.username_pw_set(CALEFACCION_ACCESS_TOKEN['DHT11'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_DHT11.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_DHT11.loop_start()
    
    procedimientos_calefaccion.medir_temperatura_humedad_ambiental()
    
    temperatura_ambiental = calefaccion_DAO.consultar_temperatura_ambiental()
    humedad_ambiental = calefaccion_DAO.consultar_humedad_ambiental()

    DHT11_data['temperatura_ambiental'] = temperatura_ambiental.valor
    DHT11_data['humedad_ambiental'] = humedad_ambiental.valor
    DHT11_data['fecha y hora'] = humedad_ambiental.fecha_hora.__str__()

    # Sending humidity and temperature data to ThingsBoard
    cliente_DHT11.publish('v1/devices/me/telemetry', json.dumps(DHT11_data), 1)

    cliente_DHT11.loop_stop()
    cliente_DHT11.disconnect()


def transmitir_estado_calefaccion():
    # Set access token
    cliente_calefaccion.username_pw_set(CALEFACCION_ACCESS_TOKEN['tapete_calefactor'])
    
    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_calefaccion.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_calefaccion.loop_start()
    
    estado_calefaccion = calefaccion_DAO.consultar_estado_calefaccion()
    
    calefaccion_data['estado_calefaccion'] = estado_calefaccion.valor
    calefaccion_data['fecha y hora'] = estado_calefaccion.fecha_hora.__str__()
    
    # Sending data to ThingsBoard
    cliente_calefaccion.publish('v1/devices/me/telemetry', json.dumps(calefaccion_data), 1)

    cliente_calefaccion.loop_stop()
    cliente_calefaccion.disconnect()


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    estado_calefaccion = calefaccion_DAO.consultar_estado_calefaccion()
    
    calefaccion_data['estado_calefaccion'] = estado_calefaccion.valor
    calefaccion_data['fecha y hora'] = estado_calefaccion.fecha_hora.__str__()
    
    #print('Connected with result code ' + str(rc))
    # Subscribing to receive RPC requests
    client.subscribe('v1/devices/me/rpc/request/+')
    # Sending current GPIO status
    client.publish('v1/devices/me/telemetry', json.dumps(calefaccion_data), 1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    # Check request method
    if data['method'] == 'consultar_estado_calefaccion':
        # Reply with GPIO status
        client.publish(msg.topic.replace('request', 'response'), calefaccion_DAO.consultar_estado_calefaccion(), 1)
    elif data['method'] == 'administrar_calefaccion':
        # Update GPIO status and reply
        procedimientos_calefaccion.administrar_calefaccion(data['params'])
        
        estado_calefaccion = calefaccion_DAO.consultar_estado_calefaccion(userdata)
        
        calefaccion_data['estado_calefaccion'] = estado_calefaccion.valor
        calefaccion_data['fecha y hora'] = estado_calefaccion.fecha_hora.__str__()
                
        client.publish(msg.topic.replace('request', 'response'), json.dumps(calefaccion_data), 1)
        client.publish('v1/devices/me/telemetry', json.dumps(calefaccion_data), 1)


# Register connect callback
cliente_tapete_calefactor.on_connect = on_connect

# Registed publish message callback
cliente_tapete_calefactor.on_message = on_message

# Set access token
cliente_tapete_calefactor.username_pw_set(CALEFACCION_ACCESS_TOKEN['tapete_calefactor'])

# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
cliente_tapete_calefactor.connect(THINGSBOARD_HOST, 1883, 60)


try:
    cliente_tapete_calefactor.loop_start()
except KeyboardInterrupt:
    print("Proceso interrumpido.")
