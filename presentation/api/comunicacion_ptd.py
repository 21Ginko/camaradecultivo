import sys
sys.path.append('/home/pi/camaradecultivo')

import infraestructure.persistence.ptd_DAO as ptd_DAO

from infraestructure.config.thingsboard_access_data import THINGSBOARD_HOST, PTD_ACCESS_TOKEN

import paho.mqtt.client as mqtt
import json

raspberry_pi_b3_data = {'estado_gestion_automatica': 0}

cliente_raspberry_pi_b3 = mqtt.Client()


def transmitir_gestion_automatica():
    # Set access token
    cliente_raspberry_pi_b3.username_pw_set(PTD_ACCESS_TOKEN['raspberry_pi_3b'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_raspberry_pi_b3.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_raspberry_pi_b3.loop_start()
    
    estado_gestion_automatica = ptd_DAO.consultar_estado_gestion_automatica()
    
    raspberry_pi_b3_data['estado_gestion_automatica'] = estado_gestion_automatica.valor
    raspberry_pi_b3_data['fecha y hora'] = estado_gestion_automatica.fecha_hora.__str__()

    cliente_raspberry_pi_b3.publish('v1/devices/me/telemetry', json.dumps(raspberry_pi_b3_data), 1)

    cliente_raspberry_pi_b3.loop_stop()
    cliente_raspberry_pi_b3.disconnect()


def transmitir_alerta(mensaje_alerta, fecha_actual):
    # Set access token
    cliente_raspberry_pi_b3.username_pw_set(PTD_ACCESS_TOKEN['raspberry_pi_3b'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_raspberry_pi_b3.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_raspberry_pi_b3.loop_start()
        
    raspberry_pi_b3_data['mensaje_alerta'] = mensaje_alerta
    raspberry_pi_b3_data['fecha y hora de alerta'] = fecha_actual.__str__()

    cliente_raspberry_pi_b3.publish('v1/devices/me/telemetry', json.dumps(raspberry_pi_b3_data), 1)

    cliente_raspberry_pi_b3.loop_stop()
    cliente_raspberry_pi_b3.disconnect()
    
    
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    estado_gestion_automatica = ptd_DAO.consultar_estado_gestion_automatica()
    
    raspberry_pi_b3_data['estado_gestion_automatica'] = estado_gestion_automatica.valor
    raspberry_pi_b3_data['fecha y hora'] = estado_gestion_automatica.fecha_hora.__str__()
    
    #print('Connected with result code ' + str(rc))
    # Subscribing to receive RPC requests
    cliente_raspberry_pi_b3.subscribe('v1/devices/me/rpc/request/+')
    # Sending current GPIO status
    cliente_raspberry_pi_b3.publish('v1/devices/me/telemetry', json.dumps(raspberry_pi_b3_data), 1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    # Check request method
    if data['method'] == 'consultar_gestion':
        estado_gestion_automatica = ptd_DAO.consultar_estado_gestion_automatica()
        # Reply with GPIO status
        cliente_raspberry_pi_b3.publish(msg.topic.replace('request', 'response'), int(estado_gestion_automatica.valor), 1)
    elif data['method'] == 'administrar_modulos':
        # Update GPIO status and reply
        orden_gestion_automatica = 0
        
        if data['params'] == True:
            orden_gestion_automatica = 1
        else:
            orden_gestion_automatica = 0   
            
        ptd_DAO.insertar_estado_gestion_automatica(orden_gestion_automatica)
        
        estado_gestion_automatica = ptd_DAO.consultar_estado_gestion_automatica()
    
        raspberry_pi_b3_data['estado_gestion_automatica'] = estado_gestion_automatica.valor
        raspberry_pi_b3_data['fecha y hora'] = estado_gestion_automatica.fecha_hora.__str__()
        
        cliente_raspberry_pi_b3.publish(msg.topic.replace('request', 'response'), int(estado_gestion_automatica.valor), 1)
        cliente_raspberry_pi_b3.publish('v1/devices/me/telemetry', int(estado_gestion_automatica.valor), 1)


# Register connect callback
cliente_raspberry_pi_b3.on_connect = on_connect
# Registed publish message callback
cliente_raspberry_pi_b3.on_message = on_message
# Set access token
cliente_raspberry_pi_b3.username_pw_set(PTD_ACCESS_TOKEN['raspberry_pi_3b'])
# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
cliente_raspberry_pi_b3.connect(THINGSBOARD_HOST, 1883, 60)


try:
    cliente_raspberry_pi_b3.loop_start()
except KeyboardInterrupt:
    print("Proceso interrumpido.")

