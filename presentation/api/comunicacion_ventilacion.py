import sys
sys.path.append('/home/pi/camaradecultivo')

import application.procedimientos_ventilacion as procedimientos_ventilacion

import infraestructure.persistence.ventilacion_DAO as ventilacion_DAO

from infraestructure.config.thingsboard_access_data import THINGSBOARD_HOST, VENTILACION_ACCESS_TOKEN

import paho.mqtt.client as mqtt
import json

detector_magnetico_data = {'apertura_puerta': 0}
MQ135_data = {'concentracion_gas': 0}

ventilador_data = {'estado_abanico': 0}
ventilador_a_data = {'estado_abanico': 0}
ventilador_b_data = {'estado_abanico': 0}
ventilador_c_data = {'estado_abanico': 0}

cliente_ventilacion = mqtt.Client()

cliente_detector_magnetico = mqtt.Client()
cliente_MQ135 = mqtt.Client()

cliente_ventilador_a = mqtt.Client()
cliente_ventilador_b = mqtt.Client()
cliente_ventilador_c = mqtt.Client()

cliente_ventilador_a.user_data_set(1)
cliente_ventilador_b.user_data_set(2)
cliente_ventilador_c.user_data_set(3)


def transmitir_apertura_puerta():
    # Set access token
    cliente_detector_magnetico.username_pw_set(VENTILACION_ACCESS_TOKEN['detector_magnetico'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_detector_magnetico.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_detector_magnetico.loop_start()
    
    procedimientos_ventilacion.detectar_apertura_puerta()
    
    apertura_puerta = ventilacion_DAO.consultar_apertura_puerta()
    
    detector_magnetico_data['apertura_puerta'] = apertura_puerta.valor
    detector_magnetico_data['fecha y hora'] = apertura_puerta.fecha_hora.__str__()

    # Sending humidity and temperature data to ThingsBoard
    cliente_detector_magnetico.publish('v1/devices/me/telemetry', json.dumps(detector_magnetico_data), 1)

    cliente_detector_magnetico.loop_stop()
    cliente_detector_magnetico.disconnect()


def transmitir_concentracion_gas():
    # Set access token
    cliente_MQ135.username_pw_set(VENTILACION_ACCESS_TOKEN['MQ-135'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_MQ135.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_MQ135.loop_start()
    
    procedimientos_ventilacion.medir_concentracion_gas()
    
    concentracion_gas = ventilacion_DAO.consultar_concentracion_gas()
    
    MQ135_data['concentracion_gas'] = concentracion_gas.valor
    MQ135_data['fecha y hora'] = concentracion_gas.fecha_hora.__str__()

    # Sending humidity and temperature data to ThingsBoard
    cliente_MQ135.publish('v1/devices/me/telemetry', json.dumps(MQ135_data), 1)

    cliente_MQ135.loop_stop()
    cliente_MQ135.disconnect() 


def transmitir_estado_ventilador(abanico):
    # Set access token
    if abanico == 1:
        ventilador_data = ventilador_a_data
        cliente_ventilacion.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_a'])
        
    if abanico == 2:
        ventilador_data = ventilador_b_data
        cliente_ventilacion.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_b'])

    if abanico == 3:
        ventilador_data = ventilador_c_data
        cliente_ventilacion.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_c'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_ventilacion.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_ventilacion.loop_start()
    
    estado_abanico = ventilacion_DAO.consultar_estado_abanico(abanico)
    
    ventilador_data['estado_abanico'] = estado_abanico.valor
    ventilador_data['fecha y hora'] = estado_abanico.fecha_hora.__str__()
    
    # Sending humidity and temperature data to ThingsBoard
    cliente_ventilacion.publish('v1/devices/me/telemetry', json.dumps(ventilador_data), 1)

    cliente_ventilacion.loop_stop()
    cliente_ventilacion.disconnect()


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    estado_abanico = ventilacion_DAO.consultar_estado_abanico(userdata)
    
    ventilador_data['estado_abanico'] = estado_abanico.valor
    ventilador_data['fecha y hora'] = estado_abanico.fecha_hora.__str__()
    #print('Connected with result code ' + str(rc))
    # Subscribing to receive RPC requests
    client.subscribe('v1/devices/me/rpc/request/+')
    # Sending current GPIO status
    client.publish('v1/devices/me/telemetry', json.dumps(ventilador_data), 1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    # Check request method
    if data['method'] == 'consultar_estado_abanico':
        # Reply with GPIO status
        client.publish(msg.topic.replace('request', 'response'), ventilacion_DAO.consultar_estado_abanico(userdata), 1)
    elif data['method'] == 'administrar_abanicos':
        # Update GPIO status and reply            
        procedimientos_ventilacion.administrar_abanicos(data['params'], userdata)
        
        estado_abanico = ventilacion_DAO.consultar_estado_abanico(userdata)
    
        ventilador_data['estado_abanico'] = estado_abanico.valor
        ventilador_data['fecha y hora'] = estado_abanico.fecha_hora.__str__()
        
        client.publish(msg.topic.replace('request', 'response'), json.dumps(ventilador_data), 1)
        client.publish('v1/devices/me/telemetry', json.dumps(ventilador_data), 1)

# Register connect callback
cliente_ventilador_a.on_connect = on_connect
cliente_ventilador_b.on_connect = on_connect
cliente_ventilador_c.on_connect = on_connect

# Registed publish message callback
cliente_ventilador_a.on_message = on_message
cliente_ventilador_b.on_message = on_message
cliente_ventilador_c.on_message = on_message

# Set access token
cliente_ventilador_a.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_a'])
cliente_ventilador_b.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_b'])
cliente_ventilador_c.username_pw_set(VENTILACION_ACCESS_TOKEN['ventilador_c'])

# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
cliente_ventilador_a.connect(THINGSBOARD_HOST, 1883, 60)
cliente_ventilador_b.connect(THINGSBOARD_HOST, 1883, 60)
cliente_ventilador_c.connect(THINGSBOARD_HOST, 1883, 60)


try:
    cliente_ventilador_a.loop_start()
    cliente_ventilador_b.loop_start()
    cliente_ventilador_c.loop_start()
except KeyboardInterrupt:
    print("Proceso interrumpido.")
