import sys
sys.path.append('/home/pi/camaradecultivo')

import comunicacion_iluminacion 
import comunicacion_ventilacion
import comunicacion_calefaccion 
import comunicacion_riego 
import comunicacion_ptd

import application.procedimientos_ptd as procedimientos_ptd

import time
import schedule

def transmitir_mediciones():

    #Sensores
    
    comunicacion_iluminacion.transmitir_incidencia_luz()
    time.sleep(1)
    
    comunicacion_iluminacion.transmitir_altura_luminaria()
    time.sleep(1)

    comunicacion_ventilacion.transmitir_apertura_puerta()
    time.sleep(1)
    
    comunicacion_ventilacion.transmitir_concentracion_gas()
    time.sleep(1)
    
    comunicacion_calefaccion.transmitir_temperatura_humedad_ambiental()
    time.sleep(1)
   
    comunicacion_riego.transmitir_humedad_suelo()
    time.sleep(1)
    
    comunicacion_riego.transmitir_nivel_agua()
    time.sleep(1)
    
    
    #Actuadores
    
    comunicacion_iluminacion.transmitir_estado_luminaria()
    time.sleep(1)
    
    comunicacion_ventilacion.transmitir_estado_ventilador(1)
    time.sleep(1)
    
    comunicacion_ventilacion.transmitir_estado_ventilador(2)
    time.sleep(1)
    
    comunicacion_ventilacion.transmitir_estado_ventilador(3)
    time.sleep(1)
    
    comunicacion_calefaccion.transmitir_estado_calefaccion()
    time.sleep(1)
    
    comunicacion_riego.transmitir_estado_riego()
    time.sleep(1)
    
    
    #Gestión automatica

    comunicacion_ptd.transmitir_gestion_automatica()    
    time.sleep(1)
    
    procedimientos_ptd.administrar_modulos()
    time.sleep(1)
    
schedule.every(5).minutes.do(transmitir_mediciones)

try:
    while True:
        schedule.run_pending() 
        time.sleep(1) 
except KeyboardInterrupt:
    pass
