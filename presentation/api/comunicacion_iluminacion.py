import sys
sys.path.append('/home/pi/camaradecultivo')

import application.procedimientos_iluminacion as procedimientos_iluminacion

import infraestructure.persistence.iluminacion_DAO as iluminacion_DAO

from infraestructure.config.thingsboard_access_data import THINGSBOARD_HOST, ILUMINACION_ACCESS_TOKEN

import paho.mqtt.client as mqtt
import json

Fotoresistencia_data = {'incidencia_luz': 0}
HCSR04_data = {'altura_luminaria': 0}
LED_data = {'estado_luminaria': 0}

cliente_Fotoresistencia = mqtt.Client()
cliente_HCSR04 = mqtt.Client()
cliente_LED = mqtt.Client()

def transmitir_incidencia_luz():
    # Set access token
    cliente_Fotoresistencia.username_pw_set(ILUMINACION_ACCESS_TOKEN['fotoresistencia'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_Fotoresistencia.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_Fotoresistencia.loop_start()
    
    procedimientos_iluminacion.medir_incidencia_luz()
    
    incidencia_luz = iluminacion_DAO.consultar_incidencia_luz()
    
    Fotoresistencia_data['incidencia_luz'] = incidencia_luz.valor
    Fotoresistencia_data['fecha y hora'] = incidencia_luz.fecha_hora.__str__()
     
    cliente_Fotoresistencia.publish('v1/devices/me/telemetry', json.dumps(Fotoresistencia_data), 1)

    cliente_Fotoresistencia.loop_stop()
    cliente_Fotoresistencia.disconnect()


def transmitir_altura_luminaria():
    # Set access token
    cliente_HCSR04.username_pw_set(ILUMINACION_ACCESS_TOKEN['HC-SR04'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_HCSR04.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_HCSR04.loop_start()
    
    procedimientos_iluminacion.medir_altura_luminaria()
    
    altura_luminaria = iluminacion_DAO.consultar_altura_luminaria()
    
    HCSR04_data['altura_luminaria'] = altura_luminaria.valor
    HCSR04_data['fecha y hora'] = altura_luminaria.fecha_hora.__str__()

    cliente_HCSR04.publish('v1/devices/me/telemetry', json.dumps(HCSR04_data), 1)

    cliente_HCSR04.loop_stop()
    cliente_HCSR04.disconnect()
    

def transmitir_estado_luminaria():
    # Set access token
    cliente_LED.username_pw_set(ILUMINACION_ACCESS_TOKEN['LED'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_LED.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_LED.loop_start()
    
    estado_luminaria = iluminacion_DAO.consultar_estado_luminaria()
    
    LED_data['estado_luminaria'] = estado_luminaria.valor
    LED_data['fecha y hora'] = estado_luminaria.fecha_hora.__str__()
    
    cliente_LED.publish('v1/devices/me/telemetry', json.dumps(LED_data), 1)

    cliente_LED.loop_stop()
    cliente_LED.disconnect()

    
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    estado_luminaria = iluminacion_DAO.consultar_estado_luminaria()
    
    LED_data['estado_luminaria'] = estado_luminaria.valor
    LED_data['fecha y hora'] = estado_luminaria.fecha_hora.__str__()
    #print('Connected with result code ' + str(rc))
    # Subscribing to receive RPC requests
    client.subscribe('v1/devices/me/rpc/request/+')
    # Sending current GPIO status
    client.publish('v1/devices/me/telemetry', json.dumps(LED_data), 1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    # Check request method
    if data['method'] == 'consultar_estado_luminaria':
        # Reply with GPIO status
        client.publish(msg.topic.replace('request', 'response'), iluminacion_DAO.consultar_estado_luminaria(), 1)
    elif data['method'] == 'administrar_iluminacion':
        # Update GPIO status and reply
        procedimientos_iluminacion.administrar_iluminacion(data['params'])
        
        estado_luminaria = iluminacion_DAO.consultar_estado_luminaria()
    
        LED_data['estado_luminaria'] = estado_luminaria.valor
        LED_data['fecha y hora'] = estado_luminaria.fecha_hora.__str__()
        
        client.publish(msg.topic.replace('request', 'response'), json.dumps(LED_data), 1)
        client.publish('v1/devices/me/telemetry', json.dumps(LED_data), 1)


# Register connect callback
cliente_LED.on_connect = on_connect
# Registed publish message callback
cliente_LED.on_message = on_message
# Set access token
cliente_LED.username_pw_set(ILUMINACION_ACCESS_TOKEN['LED'])
# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
cliente_LED.connect(THINGSBOARD_HOST, 1883, 60)


try:
    cliente_LED.loop_start()
except KeyboardInterrupt:
    print("Proceso interrumpido.")
