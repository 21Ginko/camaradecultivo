import sys
sys.path.append('/home/pi/camaradecultivo')

import application.procedimientos_riego as procedimientos_riego

import infraestructure.persistence.riego_DAO as riego_DAO

from infraestructure.config.thingsboard_access_data import THINGSBOARD_HOST, RIEGO_ACCESS_TOKEN

import paho.mqtt.client as mqtt
import json

SEN0193_data = {'humedad_suelo': 0}
medidor_agua_data = {'nivel_agua': 0}
riego_data = {'estado_riego': 0}

cliente_riego = mqtt.Client()

cliente_SEN0193 = mqtt.Client()
cliente_medidor_agua = mqtt.Client()
cliente_bomba_agua = mqtt.Client()


def transmitir_humedad_suelo():
    # Set access token
    cliente_SEN0193.username_pw_set(RIEGO_ACCESS_TOKEN['SEN0193'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_SEN0193.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_SEN0193.loop_start()
    
    procedimientos_riego.medir_humedad_suelo()
    
    humedad_suelo = riego_DAO.consultar_humedad_suelo()
    
    SEN0193_data['humedad_suelo'] = humedad_suelo.valor
    SEN0193_data['fecha y hora'] = humedad_suelo.fecha_hora.__str__()

    # Sending humidity and temperature data to ThingsBoard
    cliente_SEN0193.publish('v1/devices/me/telemetry', json.dumps(SEN0193_data), 1)

    cliente_SEN0193.loop_stop()
    cliente_SEN0193.disconnect()


def transmitir_nivel_agua():
    # Set access token
    cliente_medidor_agua.username_pw_set(RIEGO_ACCESS_TOKEN['medidor_agua'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_medidor_agua.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_medidor_agua.loop_start()
    
    procedimientos_riego.medir_nivel_agua()
    
    nivel_agua = riego_DAO.consultar_nivel_agua()
    
    estado_tanque = riego_DAO.consultar_estado_tanque()
    
    medidor_agua_data['nivel_agua'] = nivel_agua.valor
    medidor_agua_data['estado_tanque'] = estado_tanque
    medidor_agua_data['fecha y hora'] = nivel_agua.fecha_hora.__str__()

    # Sending humidity and temperature data to ThingsBoard
    cliente_medidor_agua.publish('v1/devices/me/telemetry', json.dumps(medidor_agua_data), 1)

    cliente_medidor_agua.loop_stop()
    cliente_medidor_agua.disconnect()


def transmitir_estado_riego():
    cliente_riego.username_pw_set(RIEGO_ACCESS_TOKEN['bomba_de_agua'])

    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
    cliente_riego.connect(THINGSBOARD_HOST, 1883, 60)

    cliente_riego.loop_start()
    
    estado_riego = riego_DAO.consultar_estado_riego()
    
    riego_data['estado_riego'] = estado_riego.valor
    riego_data['fecha y hora'] = estado_riego.fecha_hora.__str__()
    
    # Sending data to ThingsBoard
    cliente_riego.publish('v1/devices/me/telemetry', json.dumps(riego_data), 1)

    cliente_riego.loop_stop()
    cliente_riego.disconnect()


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    estado_riego = riego_DAO.consultar_estado_riego()
    
    riego_data['estado_riego'] = estado_riego.valor
    riego_data['fecha y hora'] = estado_riego.fecha_hora.__str__()
    #print('Connected with result code ' + str(rc))
    # Subscribing to receive RPC requests
    client.subscribe('v1/devices/me/rpc/request/+')
    # Sending current GPIO status
    client.publish('v1/devices/me/telemetry', json.dumps(riego_data), 1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    # Check request method
    if data['method'] == 'consultar_orden_riego':
        estado_riego = riego_DAO.consultar_estado_riego()
        # Reply with GPIO status
        client.publish(msg.topic.replace('request', 'response'), int(estado_riego.valor), 1)
    elif data['method'] == 'administrar_riego':
        orden_riego = 0
        # Update GPIO status and reply
        estado_riego = riego_DAO.consultar_estado_riego()
    
        riego_data['estado_riego'] = estado_riego.valor
        riego_data['fecha y hora'] = estado_riego.fecha_hora.__str__()
        
        procedimientos_riego.administrar_riego(data['params'])
        client.publish(msg.topic.replace('request', 'response'), json.dumps(riego_data), 1)
        client.publish('v1/devices/me/telemetry', json.dumps(riego_data), 1)

# Register connect callback
cliente_bomba_agua.on_connect = on_connect

# Registed publish message callback
cliente_bomba_agua.on_message = on_message

# Set access token
cliente_bomba_agua.username_pw_set(RIEGO_ACCESS_TOKEN['bomba_de_agua'])

# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
cliente_bomba_agua.connect(THINGSBOARD_HOST, 1883, 60)


try:
    cliente_bomba_agua.loop_start()
except KeyboardInterrupt:
    print("Proceso interrumpido.")
